var header = ["Name", "Market Cap", "Sales", "Profit", "Number of Employees"];
var stocks = [];

stocks.push({
    name:"Microsoft",
    marketCap: "$381.7B",
    sales: "$86.8B",
    profit: "$22.1B",
    numEmployees: 128000
});

stocks.push({
    name:"Costco Wholesale",
    marketCap: "$60.4B",
    sales: "$112.6B",
    profit: "$2.1B",
    numEmployees: 195000
});

stocks.push({
    name: "Nike",
    marketCap: "$83.1B",
    sales: "$27.8B",
    profit: "$2.7B",
    numEmployees: 56500
});

stocks.push({
    name: "Nordstrom",
    marketCap: "$15.1B",
    sales: "$12.5B",
    profit: "$734M",
    numEmployees: 67000
});

stocks.push({
    name: "Zumiez",
    marketCap: "$1.1B",
    sales: "$724.3M",
    profit: "$45.9M",
    numEmployees: 6500
});

stocks.push({
    name: "Starbucks",
    marketCap: "$61.6B",
    sales: "$16.4B",
    profit: "$2.1B",
    numEmployees: 191000

});

stocks.push({
    name: "T-Mobile US",
    marketCap: "$21.8B",
    sales: "$29.6B",
    profit: "$247M",
    numEmployees: "45000"
});

stocks.push({
    name: "Amazon.com",
    marketCap: "$144.3B",
    sales: "$89.0B",
    profit: "-$241M",
    numEmployees: 154100
});

stocks.push({
    name: "Paccar",
    marketCap: "$24.1B",
    sales: "$19.0B",
    profit: "$1.4B",
    numEmployees: 23300
});

stocks.push({
    name: "Nanostring Technologies",
    marketCap: "$254M",
    sales: "$47.6M",
    profit: "-$50M",
    numEmployees: 276
});

console.log(stocks);

function debug(item) {
    console.log(item.name);
    var el = document.getElementById("demo");
    el.innerHTML = createHeader(header);
    el.innerHTML += createDataBoard(stocks);
}

function createHeader (headers) {
    let html_str = '<div class="row header">';
    for (var i = 0; i < headers.length; i++) {
        html_str += '<div class="col heading">' + headers[i] + "</div>";
    }
    html_str += "</div>";
    return html_str;
}

function createDataBoard(data) {
    let html_str = "";
    for (var i = 0; i < data.length; i++) {
        html_str += '<div class="row">';
        html_str += '<div class="col">' + data[i]["name"] + "</div>";
        html_str += '<div class="col">' + data[i]["marketCap"] + "</div>";
        html_str += '<div class="col">' + data[i]["sales"] + "</div>";
        html_str += '<div class="col">' + data[i]["profit"] + "</div>";
        html_str += '<div class="col">' + data[i]["numEmployees"] + "</div>";
        html_str += "</div>";
    }
    return html_str;
}