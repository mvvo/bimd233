var scores = [];

// scores.push({
//   school: "",
//   conference: "",
//   overall: "",
//   last_game: ""
// });

scores.push({
  school: "Oregon",
  conference: "6-2",
  overall: "9-2",
  last_game: "L 7-38",
  letters: "ou"
});

scores.push({
  school: "Oregon State",
  conference: "5-3",
  overall: "7-4",
  last_game: "W 24-10",
  letters: "osu"
});

scores.push({
  school: "Washington State",
  conference: "5-3",
  overall: "6-5",
  last_game: "W 44-18",
  letters: "wsu"
});
scores.push({
  school: "California",
  conference: "3-4",
  overall: "4-6",
  last_game: "W 41-11",
  letters: "cal"
});

scores.push({
  school: "Washington",
  conference: "3-5",
  overall: "4-7",
  last_game: "L 17-20",
  letters: "uw"
});

scores.push({
  school: "Stanford",
  conference: "2-7",
  overall: "4-7",
  last_game: "L 11-41",
  letters: "su"
});

function mainFunction() {
  var nodeList = document.querySelectorAll("li.north-div");
  // console.log("Length Data:", scores.length, nodeList.length);
  nodeList.forEach(populateRow);
}

function populateRow(obj, i) {
  console.log("Obj:", obj, i);
  obj.innerHTML = buildRowHTML(scores[i]);
  obj.id = scores[i].letters;
}

function buildRowHTML(data) {
  let strHTML = '<div class="row">';
  strHTML += '<div class="col-sm college">' + data.school + "</div>";
  strHTML += '<div class="col-sm college">' + data.conference + "</div>";
  strHTML += '<div class="col-sm college">' + data.overall + "</div>";
  strHTML += '<div class="col-sm college">' + data.last_game + "</div>";
  return strHTML;
}

//    ASU @  OREGON11/27ESPN CLICK TO SHOW MORE INFORMATION
// WASHINGTON STATE  WASHINGTON STATE 5-3 6-5 W 44-18 ARIZ  @  WASHINGTON11/26FS1 CLICK TO SHOW MORE INFORMATION
// CALIFORNIA  CALIFORNIA 3-4 4-6 W 41-11 STAN  @  UCLA11/27FS1 CLICK TO SHOW MORE INFORMATION
// WASHINGTON  WASHINGTON 3-5 4-7 L 17-20 COL VS  WASHINGTON STATE11/26FS1 CLICK TO SHOW MORE INFORMATION
// STANFORD  STANFORD 2-7 3-8 L 11-41 CAL