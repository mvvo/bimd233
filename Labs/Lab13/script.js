var header_data = ["Flight #", "Aircraft Type", "Departure Airport", "Arrival Airport", "Departed", "Arrived", "Flight Duration"];

var flight_data = [
    {
        "Flight #": "ASA1077",
        "Aircraft Type": "A319",
        "Departure Airport": "Washington Dulles Intl (KIAD)",
        "Arrival Airport": "San Francisco Intl (KSFO)",
        "Departed": "Wed 04:32PM PST",
        "Arrived": "Wed 10:10PM PST"
    },
    {
        "Flight #": "ASA1088",
        "Aircraft Type": "A320",
        "Departure Airport": "San Francisco Intl (KSFO)",
        "Arrival Airport": "Washington Dulles Intl (KIAD)",
        "Departed": "Wed 03:58PM PST",
        "Arrived": "Wed 08:28PM PST"
    },
    {
        "Flight #": "ASA1097",
        "Aircraft Type": "A320",
        "Departure Airport": "Washington Dulles Intl (KIAD)",
        "Arrival Airport": "Los Angeles Intl (KLAX)",
        "Departed": "Wed 02:06PM PST",
        "Arrived": "Wed 07:24PM PST"
    },
    {
        "Flight #": "ASA11",
        "Aircraft Type": "B739",
        "Departure Airport": "Newark Liberty Intl (KEWR)",
        "Arrival Airport": "Seattle-Tacoma Intl (KSEA)",
        "Departed": "Wed 02:00PM PST",
        "Arrived": "Wed 07:27PM PST"
    },
    {
        "Flight #": "ASA1113",
        "Aircraft Type": "A320",
        "Departure Airport": "Will Rogers World (KOKC)",
        "Arrival Airport": "Seattle-Tacoma Intl (KSEA)",
        "Departed": "Wed 03:40PM PST",
        "Arrived": "Wed 07:11PM PST"
    }
];

function DateDiff(D1, D2) {
    let DeltaT = "";

    var Delta2 = new Date("2021-11-17" + D2.substring(3, 9) + ":00");
    var Delta1 = new Date("2021-11-17" + D1.substring(3, 9) + ":00");

    DeltaT = Delta2 - Delta1;

    return DeltaT;
}

function FormatTimeHHMMSS(dT) {
    let hr = parseInt(dT / 3600000) * -1;
    let min = parseInt((dT % 3600000) / 60000) * -1;
    let sec = parseInt((dT % 3600000 % 60000) / 1000) * -1;

    let strTime = "" + hr + ":" + min + ":" + sec;

    return strTime;
}

function build_html_header(ary_data) {
    let html_str = '<div class="row">';
    for (var i = 0; i < ary_data.length; i++) {
        html_str += '<div class="col heading">' + ary_data[i] + "</div>";
    }
    html_str += "</div>";
    return html_str;
}

function build_html_flights(data) {
    let html_str = "";
    for (var i = 0; i < data.length; i++) {
        html_str += '<div class="row">';
        html_str += '<div class="col">' + data[i]["Flight #"] + "</div>";
        html_str += '<div class="col">' + data[i]["Aircraft Type"] + "</div>";
        html_str += '<div class="col">' + data[i]["Departure Airport"] + "</div>";
        html_str += '<div class="col">' + data[i]["Arrival Airport"] + "</div>";
        html_str += '<div class="col">' + data[i]["Departed"] + "</div>";
        html_str += '<div class="col">' + data[i]["Arrived"] + "</div>";
        html_str += '<div class="col">' + FormatTimeHHMMSS(DateDiff(data[i]["Arrived"], data[i]["Departed"])) + "</div>";
        html_str += "</div>";
    }
    return html_str;
}

for (var i = 0; i < flight_data.length; i++) {
    console.log(flight_data[i]);
}

var el = document.getElementById("flight-data");
el.innerHTML = build_html_header(header_data);
el.innerHTML += build_html_flights(flight_data);