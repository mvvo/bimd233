var lis = document.querySelectorAll("li");

for (var i = 0; i < lis.length; i++) {
  console.log(lis[i]);
  lis[i].addEventListener("mouseover", function () {
    this.id = "selected";
    this.textContent = "Focused!";
    console.log("mouseover");
  });

  lis[i].addEventListener("mouseout", function () {
    this.id = "deselected";
    this.textContent = "Not Focused";
    console.log("mouseout");
  });

  lis[i].addEventListener("click", function () {
    console.log("clicked");
  });
}