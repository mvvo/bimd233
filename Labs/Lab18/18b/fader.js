$(document).ready(function () {
    $('li').css('margin', '10px');
    $('li').attr('id', 'uw');

    $('#p1 li').click(function () {
        console.log("$(this):" + $(this));
        $(this).fadeOut(2000, function () {
            console.log("fadeout complete!")
        });
    });


    $('#p2 li').click(function () {
        console.log("$(this):" + $(this));

        $(this).fadeOut(100, function () {
            console.log("fadeout complete!")
        });

        $(this).fadeIn(2000, function () {
            console.log("fadein complete!")
        });
    });


    $('#p3 li').click(function () {
        console.log("$(this):" + $(this));
        $(this).fadeTo(2000, .5, function () {
            console.log("fadeTo complete!")
        });
    });

    $('#p4 li').click(function () {
        console.log("$(this):" + $(this));
        $(this).fadeToggle(3000, function () {
            console.log("fadeToggle complete!")
        });
    });
});
