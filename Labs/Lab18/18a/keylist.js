$(document).ready(function () {
    $("li").css("id", "uw");
    $("ul").on("mouseover", "li", function () {
        console.log("mouseover:" + $(this).text());
        $(this).attr("id", "uw");
    });

    $("ul").on("mouseleave", "li", function () {
        console.log("mouseleave:" + $(this).text());
        $(this).attr("id", "uw-gold");
    });

    // reset button click
    $("button").on("click", function (e) {
        console.log("Clicked reset button!")
        $('li').remove();
     });

    // remove one TODO item
    $("ul").on("click", "li", function (e) {
        console.log("Clicked on the li:" + $(this).text());
        $(this).remove();
     });

    // keypress
    $("input").on("keypress", function (e) {
        const enterKey = 13;
        var state = "IDLE";
        var code = e.which;
        var char = String.fromCharCode(code);

        if (code === enterKey) {
            let inputVal = $('input').val();
            let insertHTML = "<li>" + inputVal + "</li>";
            console.log('key:' + code + '\tchar' + char + '\data:' + inputVal);
            $('ul').append(insertHTML);
        }
    });
});