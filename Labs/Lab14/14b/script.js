var state = "IDLE";
var cmd = "";
var statesUsed = [];
var el = document.getElementById("demo");

do {
    switch (state) {
        case "IDLE": {
            statesUsed[0] = "IDLE"
            if (cmd === "run") {
                state = "S1";
                statesUsed[statesUsed.length] = "S1";
            }
        }
            break;
        case "S1": {
            if (cmd === "next") {
                state = "S2";
                statesUsed[statesUsed.length] = "S2";
            } else if (cmd === "skip") {
                state = "S3";
                statesUsed[statesUsed.length] = "S3";
            } else if (cmd === "prev") {
                state = "S4";
                statesUsed[statesUsed.length] = "S4";
            }
        }
            break;
        case "S2": {
            if (cmd === "next") {
                state = "S3";
                statesUsed[statesUsed.length] = "S3";
            } else if (cmd === "skip") {
                state = "S4";
                statesUsed[statesUsed.length] = "S4";
            } else if (cmd === "prev") {
                state = "S1";
                statesUsed[statesUsed.length] = "S1";
            }
        }
            break;
        case "S3": {
            if (cmd === "next") {
                state = "S4";
                statesUsed[statesUsed.length] = "S4";
            } else if (cmd === "skip") {
                state = "S1";
                statesUsed[statesUsed.length] = "S1";
            } else if (cmd === "prev") {
                state = "S2";
                statesUsed[statesUsed.length] = "S2";
            } else if (cmd === "home") {
                state = "IDLE";
                statesUsed[statesUsed.length] = "IDLE";
            }
        }
            break;
        case "S4": {
            if (cmd === "next") {
                state = "S1";
                statesUsed[statesUsed.length] = "S1";
            } else if (cmd === "skip") {
                state = "S2";
                statesUsed[statesUsed.length] = "S2";
            } else if (cmd === "prev") {
                state = "S3";
                statesUsed[statesUsed.length] = "S3";
            }
        }
            break;
    }

    cmd = getUserInput();
    if (cmd === 'exit' || cmd === 'quit') {
        el.innerHTML(statesUsed);
    }
} while (cmd !== 'exit' || cmd !== 'quit');

function getUserInput() {
    let cmd = prompt("State:" + state, "next");
    return cmd;
};

function getStatesUsed() {
    let html_str = "";
    
    for (var i = 0; i < statesUsed.length; i++) {
        html_str += "<p>";
        html_str += statesUsed[i];
        html_str += "</p>";
    }

    return html_str;
}

console.log(statesUsed);