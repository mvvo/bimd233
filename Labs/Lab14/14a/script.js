var headerData = ["Day", "High", "Low", "Average"];

var weatherData = [
    { "day": "Friday", "hi": 82, "lo": 55 },
    { "day": "Saturday", "hi": 75, "lo": 52 },
    { "day": "Sunday", "hi": 69, "lo": 52 },
    { "day": "Monday", "hi": 69, "lo": 48 },
    { "day": "Tuesday", "hi": 68, "lo": 51 },
    { "day": "All 5", "hi": 72.6, "lo": 51.6 }
];

var averagePerDay = [];

function getAveragePerDay(initialValue, nextValue, i) {
    let avg1 = (initialValue["hi"] + initialValue["lo"]) / 2;
    let avg2 = (nextValue["hi"] + nextValue["lo"]) / 2;
    averagePerDay[i - 1] = avg1;
    averagePerDay[i] = avg2;


    console.log("i: " + i, initialValue, avg1, avg2, nextValue);
    return nextValue;
}

weatherData.reduce(getAveragePerDay);
console.log(averagePerDay);

averagePerDay[5] = 62.1;
console.log(averagePerDay)

function buildWeatherHeader(headerData) {
    let html_str = '<div class="row header">';
    for (var i = 0; i < headerData.length; i++) {
        html_str += '<div class="col heading">' + headerData[i] + "</div>";
    }
    html_str += "</div>";
    return html_str;
}

console.log(headerData);

function buildWeatherTable(data, avg) {
    let html_str = "";
    for (var i = 0; i < data.length; i++) {
        html_str += '<div class="row">';
        html_str += '<div class="col">' + data[i]["day"] + "</div>";
        html_str += '<div class="col">' + data[i]["hi"] + "</div>";
        html_str += '<div class="col">' + data[i]["lo"] + "</div>";
        html_str += '<div class="col">' + avg[i] + "</div>";
        html_str += "</div>";
    }
    return html_str;
}

var el = document.getElementById("weatherData");
el.innerHTML = buildWeatherHeader(headerData);
el.innerHTML += buildWeatherTable(weatherData, averagePerDay)