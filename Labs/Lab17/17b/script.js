// $(document).ready(function () {
// --------- jQuery Data Section ---------
var book1 = {
    title: "A Thousand Brains: A New Theory of Intelligence",
    author: "Jeff Hawkins",
    image: "https://images-na.ssl-images-amazon.com/images/I/41OZTG+1b3L._SX321_BO1,204,203,200_.jpg"
};
var book2 = {
    title: "The Code Breaker: Jennifer Doudna, Gene Editing, and the Future of the Human Race",
    author: "Walter Isaacson",
    image: "https://d28hgpri8am2if.cloudfront.net/book_images/onix/cvr9781982115852/the-code-breaker-9781982115852_xlg.jpg"
};
var book3 = {
    title: "Klara and the Sun",
    author: "Kazuo Ishiguro",
    image: "https://images-na.ssl-images-amazon.com/images/I/711cXHTd3+L.jpg"
};
var book4 = {
    title: "Hamnet",
    author: "Maggie O’Farrell",
    image: "https://images-na.ssl-images-amazon.com/images/I/91gPEHfc7sL.jpg"
};
var book5 = {
    title: "Project Hail Mary",
    author: "Andy Weir",
    image: "https://images-na.ssl-images-amazon.com/images/I/91ieFVjGmVL.jpg"

};
var books = new Array;
books.push(book1);
books.push(book2);
books.push(book3);
books.push(book4);
books.push(book5);
var img_ref = {
    url: 'https://media.gatesnotes.com/-/media/Images/Articles/About-Bill-Gates/Holiday-Books-2021/holiday_books_2021_20210827_blog-roll_800x494.ashx',
    src: 'https://gatesnot.es/3lk3sWK',
    alt: 'Bill Gates',
    height: 100, // orig 401 by 4:1
    width: 133 // orig 534 by 4:1  
};
var reference = {
    url: 'https://www.gatesnotes.com/About-Bill-Gates/Holiday-Books-2021',
    src: 'https://gatesnot.es/3p8dsDv',
    alt: 'Gates Books',
    text: '5 books I loved reading this year'
}
// --------- jQuery Data Section ---------
// --------- jQuery Code Section ---------
$('#bg-pic').html('<img src="' + img_ref.src + '" alt="' + img_ref.alt + '" height="' + img_ref.height + 'px" width="' + img_ref.width + 'px">');
$('#bg-5books').html('<a href="' + reference.src + '" alt="' + reference.alt + '">' + reference.text + '</a>');
// apply bootstrap list group classes
$('ol').addClass("list-group");
$('li').addClass("list-group-item");
$('li').each(function (i) {
    // this.innerText = // your code to pull values from the array of objects here
    this.innerText = (i + 1) + '.' + ' "' + books[i].title + '"' + " by " + books[i].author;
});
$('li').each(function (i) {
    // add your row striping code here
    if ((i % 2) > 0) {
        $(this).attr('id', 'odd');
    } else {
        $(this).attr('id', 'even');
    }
});
// --------- jQuery Code Section ---------
// });

var imgs = [];

objImg1 = {
    url1: "https://media.gatesnotes.com/-/media/Images/Books/A-Thousand-Brains/a_thousand_brains_20211116_article-hero_1200x564.ashx"
};

objImg2 = {
    url2: "https://media.gatesnotes.com/-/media/Images/Books/The-Code-Breaker/codebreaker_2021_20211116_article-hero_1200x564.ashx"
};

objImg3 = {
    url3: "https://media.gatesnotes.com/-/media/Images/Books/Klara-and-the-Sun/klara_and_the_sun_20210827_article-hero_1200x564.ashx"
};

objImg4 = {
    url4: "https://media.gatesnotes.com/-/media/Images/Books/Hamnet/hamnet_20210827_article-hero_1200x564.ashx"
};

const url5 =
    "https://media.gatesnotes.com/-/media/Images/Books/Project-Hail-Mary/project_hail_mary_20210827_article-hero_1200x564.ashx";

const short_url5 = "https://gatesnot.es/3Gvg4ST";

objImg5 = {
    longUrl: url5,
    shortUrl: short_url5
};

images = [];

images.push(objImg1);
images.push(objImg2);
images.push(objImg3);
images.push(objImg4);
images.push(objImg5);

// Create the iteration action needed to populate the DOM (e.g innerHTML methods, etc.)
images.forEach(function (item) {
    
    console.log(item);
});