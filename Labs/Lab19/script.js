$(document).ready(function () {
    const base_url = "https://api.weather.gov/stations/";
    const endpoint = "/observations/latest";

    // weather update button click
    $('#getwx').on('click', function (e) {
        var mystation = $('input#station-id').val();
        var myurl = base_url + mystation + endpoint;
        $('input#my-url').val(myurl);

        // clear out any previous data
        $('ul li').each(function () {
            // enter code to clear each li
            $(this).remove();
        });

        console.log("Cleared Elements of UL");

        // execute AJAX call to get and render data
        $.ajax({
            url: myurl,
            dataType: "json",
            success: function (data) {
                console.log(data);
                var tempC_data = data.properties.temperature.value;
                var wind_speed_data = data.properties.windSpeed.value;
                if (tempC_data !== null && wind_speed_data !== null) {
                    var tempF = (tempC_data * 9 / 5 + 32).toFixed(1);
                    let tempC = tempC_data.toFixed(1);
                    var weather_condition = data.properties.textDescription;
                    var wind_direction = data.properties.windDirection.value;
                    var wind_speed_kts = (wind_speed_data * 0.539957).toFixed(1);
                    var weather_icon = data.properties.icon;
                    // get wind info and convert m/s to kts
                    // var windDirection = 
                    // var windSpeed = 
                    // uncomment this if you want to dump full JSON to textarea
                    var myJSON = JSON.stringify(data);
                    $('textarea').val(myJSON);
                    var str = "<li>Current temperature: " + tempC + "C " + tempF + "F"
                        + "</li>" + '<li> <img src="' + weather_icon + '">' +
                        " Weather Condition: " + weather_condition + "</li>"
                        + "<li>Current Speed and Direction: " + wind_speed_kts + " knots going towards "
                        + wind_direction + "°";
                    $('ul').append(str);
                    $('ul li').attr('class', 'list-group-item');
                    // add additional code here for the Wind direction, speed, weather conditions and icon image
                } else {
                    window.alert("There was an error in the data being processed. Please try again at a different time.")
                }
            }
        });
    });
});