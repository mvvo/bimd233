var car1 = ["Mazda", "MX-5", 1993, 15650];
var car2 = ["Porsche", "911 Carrera S (993)", 1997, 124252];
var car3 = ["Lambourghini", "Countach", 1982, 118000];
var car4 = ["Ferrari", "Testarossa", 1990, 150000];
var car5 = ["Mazda", "RX-7", 1992, 29306];
var cars = [car1, car2, car3, car4, car5];

function displayRow(el, r, data) {
    const N = data[0].length;
    el.innerHTML += "<div class = 'row'>";
    for (var i = 0; i < N; i++) {
        el.innerHTML += "<div class = 'col'> " + data[r][i] + " </div>";
    }
    el.innetHTML += "</div>";
}

function displayAllRows(data) {
    let el = document.getElementById("data");
    const C = data.length;

    for (var i = 0; i < C; i++) {
        displayRow(el, i, data);
    }
}

displayAllRows(cars);