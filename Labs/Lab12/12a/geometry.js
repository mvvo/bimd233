var radii = [];
const NUM_RADII = 3;

function calcCircleGeometries(radius) {
    const pi = Math.PI;
    var area = pi * radius * radius;
    var circumference = 2 * pi * radius;
    var diameter = 2 * radius;
    var geometries = [area, circumference, diameter];
    return geometries;
}

for (var i = 0; i < NUM_RADII; i++) {
    radii[i] = Math.random() * 100;
    let r = radii[i];
    // console.log(i, r, calcCircleGeometries(r));
}

function printAllRows(data) {
    const N = data.length;
    for (var i = 0; i < N; i++) {
        let r = data[i];
        // console.log(r, calcCircleGeometries(r));
        let el = document.getElementById("data");
        renderRow(el, r, calcCircleGeometries(r));
    }
}

// This function is going to render HTML
function renderRow(el, r, geoms) {
    el.innerHTML += "<div class = 'row'>";
    el.innerHTML += "<div class = 'col'> " + r + "</div>";
    el.innerHTML += "<div class = 'col'> " + geoms[0].toFixed(3) + " </div>";
    el.innerHTML += "<div class = 'col'> " + geoms[1].toFixed(3) + " </div>";
    el.innerHTML += "<div class = 'col'> " + geoms[2].toFixed(3) + " </div>";
    el.innetHTML += "</div>";
}

printAllRows(radii);
